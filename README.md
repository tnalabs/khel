# Khel: Learn on the Command-Line with AI-Generated Flashcards and Spaced Repetition

![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)

## Project Overview

Khel is an open-source project developed by The New Auroran Labs. It leverages
artificial intelligence to generate flashcards and uses the principle of spaced
repetition to optimize learning with AI aided reviews. All of this delivered
right to your command-line.

## Getting Started

Khel is written in Python.  Quickest way to run it is creating a virtualenv,
pulling deps, and configurting.

```sh
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python -m khel
```

This should give this output:

```
% python -m khel
usage: __main__.py [-h] [--output OUTPUT] [--config CONFIG] deck
__main__.py: error: the following arguments are required: deck
```

There is a config sample in `khel.conf.sample`.  Get an [openai API
access](https://openai.com/blog/openai-api), grab a token a store it in your
configuration file. **This is insecure**, make sure you're OK saving your token
in a file.

Now you can create a deck:

```
python -m khel --config kehl.conf animals.yaml
```

### Adding facts

Open `animals.yaml` with your favorite editor and add some facts:

```
cards:
  - front: "What is the largest land animal?"
    back: "Elephant"
  - front: "Which animal is known for its black and white stripes?"
    back: "Zebra"
  - front: "What is the fastest land animal?"
    back: "Cheetah"
  - front: "Which animal can be found in both the Arctic and the Antarctic?"
    back: "Penguin"
  - front: "What is the largest species of bear?"
    back: "Polar bear"
  - front: "Which animal is famous for its long neck?"
    back: "Giraffe"
  - front: "What is the national bird of the United States?"
    back: "Bald eagle"
  - front: "Which animal is known for its distinctive black and white fur and is native to China?"
    back: "Giant panda"
  - front: "What is the world's largest species of shark?"
    back: "Whale shark"
  - front: "Which animal is often called the 'king of the jungle'?"
    back: "Lion"
```

### Reviewing

Run `python -m khel animals.yaml`.

## Features

- **AI-Generated Flashcards**: Khel uses AI to generate high-quality, relevant
  flashcards for effective learning.
- **Spaced Repetition**: Learning sessions incorporate the principle of spaced
  repetition, using AI to evaluate your recall score and presenting flashcards
  at intervals optimized for learning efficiency.
- **Command-Line Interface**: Learn straight from your terminal. Easily plugin
  tools for other learning media.

## Contributing

We welcome contributions from the community.

## License

This project is licensed under the terms of of the terms of the [GNU General
Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Contact

If you have any questions, feel free to reach out to us at [The New Auroran Labs](https://www.tnalabs.ai).
