# khel - learn on the command-line with spaced repetition
# © 2023  J. Victor Duarte Martins <jvdm@sdf.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Implements models for learning stages using spaced repetition.

Each stage represents a different phase in the learning process.  There are
currently two: ``LearningStage`` for facts that are new and being learned.  And,
``ReviewStage`` for facts that are considered learned.

Each class is immutable, and they roughly model statei in a state machine where
which embeds SuperMemo 2 spaced repetition algorithm.
"""

from datetime import timedelta

import attr


def validate_review(review_quality):
    """Raise exceptions if review quality is invalid."""
    if review_quality < 0 or review_quality > 5:
        raise ValueError(
            f"invalid review_quality: {review_quality} (should be >= 0 and <= 5)"
        )


@attr.frozen
class ReviewStage:
    """A learning stage for facts that are considered learned."""

    interval_days: int     = 0
    repetitions: int       = 0
    easiness_factor: float = 2.5

    @property
    def interval(self):
        return timedelta(days=self.interval_days)

    def next_stage(self, review_quality: float):
        """Create a new interval based on the review quality."""
        validate_review(review_quality)
        if review_quality < 3:
            interval_days = 0
            repetitions = 0
            easiness_inc = (review_quality - 5) * 0.1
        else:
            if self.repetitions == 0:
                interval_days = 1
            elif self.repetitions == 1:
                interval_days = 3
            else:
                interval_days = round(self.interval.days * self.easiness_factor)
            repetitions = self.repetitions + 1
            easiness_inc = 0.1 - (5 - review_quality) * (0.08 + (5 - review_quality) * 0.02)
        easiness_factor = max(1.3, self.easiness_factor + easiness_inc)
        return attr.evolve(
            self,
            interval_days=interval_days,
            repetitions=repetitions,
            easiness_factor=easiness_factor,
        )


@attr.frozen
class LearnStage:
    """A learning stage for facts that are new and being learned."""

    interval_minutes:  int   = 0
    repetitions:       int   = 0
    quality_threshold: float = 4

    @property
    def interval(self):
        return timedelta(minutes=self.interval_minutes)

    def easiness_factor(self, review_quality: float) -> float:
        return 1.0 + (review_quality / 2)

    def next_stage(self, review_quality: float):
        """Create a new learning interval based on the review quality."""
        validate_review(review_quality)
        if review_quality >= self.quality_threshold:
            # Transition to review stage.
            return ReviewStage()
        # Update the learning interval.
        if self.repetitions == 0:
            interval_minutes = 10
        else:
            interval_minutes = self.interval_minutes * self.easiness_factor(review_quality)
        repetitions = self.repetitions + 1
        return attr.evolve(
            self,
            interval_minutes=interval_minutes,
            repetitions=repetitions,
        )
