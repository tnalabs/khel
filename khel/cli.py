# khel - learn on the command-line with spaced repetition
# © 2023  J. Victor Duarte Martins <jvdm@sdf.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import argparse
import configparser
from datetime import datetime
import json
import os
import readline # pylint: disable=unused-import

import attr
import openai

from .cards import CardDeck

@attr.frozen
class ResponseQuality:

    score: float  = 0.0
    feedback: str = ""

    @classmethod
    def from_json(cls, json_s):
        json_s = json_s.strip()
        if not json_s:
            raise ValueError("empty json string")
        try:
            quality_d = json.loads(json_s)
        except:
            print(json_s)
            raise
        quality_d['score'] = float(quality_d.get('score', 0.0))
        # Filter unknown fields.
        fields = set(f.name for f in attr.fields(cls))
        return cls(**{k: v for k, v in quality_d.items() if k in fields})

    @property
    def emoji(self):
        return ["💀", "⚫", "🔴", "🟠", "🟡", "🟢"][round(self.score)]


def review_card(card):
    print(f"{'🆕' if card.is_new else '💭'}  {card.front}")
    start_time = datetime.now()
    user_recall = input("📝  ")
    seconds = (datetime.now() - start_time).total_seconds() - (len(card.front.split()) / 2)
    quality = ResponseQuality()
    if user_recall:
        # pylint: disable=line-too-long
        messages = [
            {
                "role": "system",
                "content": """You are an AI language model, and your task is to assess the user's recall ability from 0% to 100% based on the following scale:

- Score 1 (0-50%): The recall response is insufficient, as it ranges from mostly unrelated
  to having some relevant content with multiple inaccuracies or incomplete information.

- Score 2 (51-70%): The recall response is moderately accurate and meaningful, containing
  most of the key information and ideas with only minor errors or omissions. Articulation
  is decent.

- Score 3 (71-90%): The recall response is accurate, complete, and well-articulated, with
  minor to very minor differences in wording or organization compared to the original
  information.

- Score 4 (91-95%): The recall response is nearly perfect, accurately conveying
  all the information and ideas from the original source with only negligible
  differences in wording or organization.

- Score 5 (96-100%): The recall response is outstanding, accurately conveying all the
  information and ideas from the original source, it is well-articulated, although the
  wording or organization might be different from the original..

You always respond with one line JSON string.
                """,
            },
            {
                "role": "user",
                "content": f"""Here is the flashcard information:

Front: {card.front}
Back: {card.back}
                """,
            },
            {
                "role": "user",
                "content": f"""The user response is: {user_recall}

Respond with only only line that includes a single JSON object, containing:

1. "score": the score.
2. "feedback": a explanation and feedback to the user to help the user improve his recalling in the next
   review.
                """
            },
        ]
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=messages,
            n=1,
            stop=None,
            temperature=1.2,
        )
        content = response.choices[0]['message']['content']
        quality = ResponseQuality.from_json(content)
    card = card.review(quality.score)
    print(f"""
{quality.emoji} {quality.feedback} ({quality.score})

💬  {card.back}
🫵  {user_recall}
{quality.emoji} {quality.score} | 🕒 {seconds:.2f}s | 📅 {card.stage.interval}d | 🔁 {card.stage.repetitions}
""")
    return card


def parse_args():
    parser = argparse.ArgumentParser(
        description="recall and retain information using spaced repetition")
    parser.add_argument(
        "deck",
        help="the flashcard deck")
    parser.add_argument(
        "--output", "-o",
        type=str,
        help="write reviewed decks to a different file")
    xdg_config_home = os.environ.get('XDG_CONFIG_HOME', os.path.expanduser('~/.config'))
    khel_conf = os.path.join(xdg_config_home, "khel", "khel.conf")
    parser.add_argument(
        "--config", "-c",
        type=argparse.FileType('r'),
        default=khel_conf,
        help="Location of khel configuration file (default: %(default)s).")
    args = parser.parse_args()
    if not args.output:
        args.output = args.deck
    config = configparser.ConfigParser()
    config.read_file(args.config)
    args.config.close()
    openai.api_key = config.get('openai', 'api_key')
    return args


def main(args):
    deck = CardDeck.open_from_yaml(args.deck)
    now = datetime.now()
    while True:
        card_id, card = deck.get_next_to_review(now)
        if card_id is None:
            # No more cards to review.
            break
        try:
            input("Press <enter> to show the next card.")
        except (EOFError, KeyboardInterrupt):
            print()
            return
        deck.replace_card(card_id, review_card(card))
        deck.flush_to_yaml(args.output)
    deck.flush_to_yaml(args.output)
