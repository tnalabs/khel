# khel - learn on the command-line with spaced repetition
# © 2023  J. Victor Duarte Martins <jvdm@sdf.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from datetime import datetime
import os

import attr
import yaml

from . import stages


@attr.frozen
class Card:
    """
    Handles the data related to a flashcard.

    A Card contains a front (the question or prompt) and back (the answer or response), as
    well as the review-related data such as the stage and last_review.  It provides
    methods to review the card, check if it's new, and decide if it should be reviewed at
    a certain time.

    The Card doesn't need to know about the specifics of the spaced repetition algorithm,
    it just knows that it has a stage which can provide the next review interval.
    """

    front:       str      = attr.field()
    back:        str      = attr.field()
    stage                 = attr.field(default=stages.LearnStage())
    last_review: datetime = attr.field(default=datetime.min)

    @property
    def is_new(self) -> bool:
        return self.last_review == datetime.min

    def review(self, review_quality: float, review_time: datetime = None):
        """Create a new card to be reviewed in the future."""
        if review_time is None:
            review_time = datetime.now()
        stage = self.stage.next_stage(review_quality)
        return attr.evolve(
            self,
            stage=stage,
            last_review=review_time)

    def should_review(self, when: datetime) -> bool:
        """Returns true if `when` is too soon for to review this card."""
        next_review_date = self.last_review + self.stage.interval
        return when.date() >= next_review_date.date()

    def to_dict(self) -> dict[str,dict]:
        d = attr.asdict(self)
        stage = d['stage']
        d['stage'] = {
            'type': type(self.stage).__name__,
            'data': stage,
        }
        return d

    @classmethod
    def from_dict(cls, d: dict[str,dict]):
        if 'stage' in d:
            stage_class = getattr(stages, d['stage']['type'])
            d['stage'] = stage_class(**d['stage']['data'])
        return cls(**d)


@attr.s
class CardDeck:
    """Deck of flashcards that can be persisted."""

    cards: dict[str,Card] = attr.ib(factory=dict)

    @classmethod
    def open_from_yaml(cls, filename: str) -> 'CardDeck':
        if os.path.exists(filename):
            with open(filename, 'r', encoding='utf-8') as file:
                data = yaml.safe_load(file)
                if not data:
                    raise ValueError("no deck found: {filename}")
            cards = {i: Card.from_dict(v) for i, v in enumerate(data.setdefault('cards', []))}
        else:
            cards = {}
        return cls(cards)

    def get_next_to_review(self, when: datetime) -> tuple[str,Card]:
        for i, card in self.cards.items():
            if card.should_review(when):
                return i, card
        return None, None

    def replace_card(self, i: int, card: Card) -> None:
        self.cards[i] = card

    def flush_to_yaml(self, filename) -> None:
        data = {'cards': [card.to_dict() for card in self.cards.values()]}
        with open(filename, 'w', encoding='utf-8') as file:
            yaml.dump(data, file)
