# khel - learn on the command-line with spaced repetition
# © 2023  J. Victor Duarte Martins <jvdm@sdf.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from datetime import (
    datetime,
    timedelta,
)
from unittest import mock

import attr

from khel import stages
from khel.cards import Card


def test_card_is_new():
    card = Card('front', 'back')
    assert card.is_new

    card = attr.evolve(card, last_review=datetime.now())
    assert not card.is_new


def test_card_review():
    mock_stage = mock.create_autospec(stages.LearnStage, instance=True)
    mock_next_stage = mock.Mock()
    mock_stage.next_stage.return_value = mock_next_stage

    card = Card('front', 'back', stage=mock_stage)
    now = datetime.now()
    card = card.review(3, now)

    mock_stage.next_stage.assert_called_once_with(3)
    assert not card.is_new
    assert card.stage == mock_next_stage
    assert card.last_review == now


def test_card_should_review():
    now = datetime.now()
    future = now + timedelta(days=2)
    card = Card('front', 'back', last_review=future)

    assert not card.should_review(now)

    card = attr.evolve(card, last_review=future)
    assert card.should_review(future)


def test_card_to_and_from_dict():
    card = Card('front', 'back')
    d = card.to_dict()
    assert d == {
        'front': 'front',
        'back': 'back',
        'stage': {
            'type': 'LearnStage',
            'data': {'interval_minutes': 0, 'repetitions': 0, 'quality_threshold': 4},
        },
        'last_review': datetime.min,
    }
    assert Card.from_dict(d) == card
