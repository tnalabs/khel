# khel - learn on the command-line with spaced repetition
# © 2023  J. Victor Duarte Martins <jvdm@sdf.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import pytest

from khel.stages import LearnStage, ReviewStage, validate_review


def test_validate_review():
    with pytest.raises(ValueError):
        validate_review(-1)
    with pytest.raises(ValueError):
        validate_review(6)
    validate_review(0)
    validate_review(5)


def test_learning_stage():
    lst = LearnStage()

    assert lst.interval_minutes == 0
    assert lst.repetitions == 0
    assert lst.quality_threshold == 4
    assert lst.interval.total_seconds() == 0

    lst = lst.next_stage(3.5)
    assert lst.interval_minutes == 10
    assert lst.repetitions == 1

    lst = lst.next_stage(5)
    assert isinstance(lst, ReviewStage)


def test_learning_stage_easiness_and_interval_factor():
    lst = LearnStage()

    assert lst.interval_factor(4) == 3.0
    assert lst.easiness_factor(5) == 2.5
    assert lst.easiness_factor(3) == 2.3


def test_review_stage():
    rst = ReviewStage()

    assert rst.interval_days == 0
    assert rst.repetitions == 0
    assert rst.easiness_factor == 2.5
    assert rst.interval.total_seconds() == 0

    rst = rst.next_stage(3)
    assert rst.interval_days == 1
    assert rst.repetitions == 1
    assert rst.easiness_factor == 2.36

    rst = rst.next_stage(3)
    assert rst.interval_days == 3
    assert rst.repetitions == 2
    assert round(rst.easiness_factor, 2) == 2.22

    rst = rst.next_stage(5)
    assert rst.interval_days >= 3
    assert rst.repetitions == 3
    assert round(rst.easiness_factor, 2) == 2.32

    rst = rst.next_stage(2)
    assert rst.interval_days == 0
    assert rst.repetitions == 0
    assert rst.easiness_factor <= 2.5
